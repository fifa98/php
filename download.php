<?php
  
  /*
  STARÉ ŘEŠENÍ
  
  require_once "../Classes/PHPExcel.php";
  
  $objPHPExcel = new PHPExcel();
  $objPHPExcel->getActiveSheet()->setCellValue('A1', 'hello world!');
  $objPHPExcel->getActiveSheet()->setTitle('Chesse1');
  
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="helloworld.xlsx"');
  header('Cache-Control: max-age=0');
  
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $objWriter->save('php://output');
  */
  
//export.php  
  include '../includes/dbh.inc.php';
  $output = '';
  if(isset($_POST["export"]))
  {
   $sql = "SELECT * FROM offers ORDER BY id DESC";
   $result = mysqli_query($conn, $sql);
   $resultCheck = mysqli_num_rows($result);
   if($resultCheck > 0)
   {
    $output .= '
      <table id="nabidka" class="table table-responsive table-bordered">
      <thead class="thead">
        <tr>
          <th>#</th>
          <th>Jméno a příjmení</th>
          <th>Email</th>
          <th>Telefon</th>
          <th>Typ nemovitosti</th>
          <th>Typ nabídky</th>
          <th>Vlastnictví</th>
          <th>Region</th>
          <th>Město</th>
          <th>PSČ</th>
          <th>Ulice, č.p.</th>
          <th>Fotografie</th>
          <th>Popis nemovitosti</th>
          <th>Maximální cena</th>
          <th>Měna</th>
          <th>Za</th>
        </tr>
      </thead>
      <tbody class="tbody">
    ';
    while($row = mysqli_fetch_array($result)) {
        $output .= '<tr><td>' . htmlspecialchars($row["id"]) .'
                    </td><td>' . htmlspecialchars($row["name"]) .'
                    </td><td>' . htmlspecialchars($row["email"]) .'
                    </td><td>' . htmlspecialchars($row["mobile"]) .'
                    </td><td>' . htmlspecialchars($row["property_type"]) .'
                    </td><td>' . htmlspecialchars($row["offer_type"]) .'
                    </td><td>' . htmlspecialchars($row["ownership"]) .'
                    </td><td>' . htmlspecialchars($row["region"]) .'
                    </td><td>' . htmlspecialchars($row["city"]) .'
                    </td><td>' . htmlspecialchars($row["postal_code"]) .'
                    </td><td>' . htmlspecialchars($row["street"]) .'
                    </td><td>' . htmlspecialchars($row["property_photos"]) .'
                    </td><td>' . htmlspecialchars($row["description"]) .'
                    </td><td>' . htmlspecialchars($row["max_cost"]) .'
                    </td><td>' . htmlspecialchars($row["currency"]) .'
                    </td><td>' . htmlspecialchars($row["per"]) .'
                    </td></tr>';
    }
    $output .= '</tbody></table>';
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename=exporttable.xls');
    echo $output;
   }
  }
  
?>